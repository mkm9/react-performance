import React from "react";
//  write code in hooks way

const updateArray = (list, updated) => {
    const index = list.findIndex(item => item.id === updated.id);
    return [
        ...list.slice(0, index),
        updated,
        ...list.slice(index + 1)
    ]
}
const deleteArrayById = (list, id) => {
    const index = list.findIndex(item => item.id === id);
    return [
        ...list.slice(0, index),
        ...list.slice(index + 1)
    ]
}

// old way

/* const Task = () => {
    const [newTask, setNewTask] = React.useState({ name: "", id: "" });
    const [taskList, setTaskList] = React.useState([]);

    const addToTask = React.useCallback(() => {
        if (newTask.name === "")
            return console.log("please enter a task name");
        if (newTask.id) {
            setTaskList(updateArray(taskList, newTask));
        } else {
            setTaskList([...taskList, { name: newTask.name, id: Date.now() }]);
        }
        setNewTask({ name: "", id: "" });
    }, [taskList, newTask]);
    const onEditTask = React.useCallback((val) => {
        setNewTask(val)
    }, []);
    const onDeleteTask = React.useCallback((val) => {
        setTaskList(deleteArrayById(taskList, val.id))
    }, [taskList]);

    return (
        <>
            <input
                value={newTask.name}
                onChange={(event) => { setNewTask({ ...newTask, name: event.target.value }) }}
            />
            <button onClick={addToTask}>{newTask.id ? "UPDATE" : "Add"}</button>

            <section>
                <ul>
                    {taskList.map((eachTask) => {
                        return (<li key={eachTask.id} >
                            {eachTask.name}
                            <MyButton value={eachTask}
                                onClick={onEditTask}>
                                EDIT
                                </MyButton>
                            <MyButton value={eachTask}
                                onClick={onDeleteTask}>
                                DELETE
                                </MyButton>
                        </li>)
                    })}
                </ul>
            </section>
        </>
    )
} */

// new way
const initialState = []
function taskReducer(state, action) {
    switch (action.type) {
        case 'ADD':
            return [...state, { name: action.task.name, id: Date.now() }];
        case 'UPDATE':
            return updateArray(state, action.task);
        case 'DELETE':
            return deleteArrayById(state, action.id);
        default:
            throw new Error();
    }
}

const Task = () => {
    const [newTask, setNewTask] = React.useState({ name: "", id: "" });
    const [taskList, dispatch] = React.useReducer(taskReducer, initialState);

    const addToTask = React.useCallback(() => {
        if (newTask.name === "")
            return console.log("please enter a task name");
        if (newTask.id) {
            // setTaskList(updateArray(taskList, newTask));
            dispatch({ type: "UPDATE", task: newTask });
        } else {
            // setTaskList([...taskList, { name: newTask.name, id: Date.now() }]);
            dispatch({ type: "ADD", task: newTask });
        }
        setNewTask({ name: "", id: "" });
    }, [newTask]);
    const onEditTask = React.useCallback((val) => {
        setNewTask(val)
    }, []);
    const onDeleteTask = React.useCallback((val) => {
        // setTaskList(deleteArrayById(taskList, val.id))
        dispatch({ type: "DELETE", id: val.id });
    }, []);

    return (
        <>
            <input
                value={newTask.name}
                onChange={(event) => { setNewTask({ ...newTask, name: event.target.value }) }}
            />
            <button onClick={addToTask}>{newTask.id ? "UPDATE" : "Add"}</button>

            <section>
                <ul>
                    {taskList.map((eachTask) => {
                        return (<li key={eachTask.id} >
                            {eachTask.name}
                            <MyButton value={eachTask}
                                onClick={onEditTask}>
                                EDIT
                                </MyButton>
                            <MyButton value={eachTask}
                                onClick={onDeleteTask}>
                                DELETE
                                </MyButton>
                        </li>)
                    })}
                </ul>
            </section>
        </>
    )

}


const MyButton = React.memo((props) => {
    console.log("MyButton", props.value.name, props.children)
    return <button style={{ marginLeft: "10px" }}
        onClick={() => props.onClick(props.value)}>
        {props.children}
    </button>
})

export default Task;

