import React from "react";
// talk anout pure component and React.memo

function calculation(num) {
  return num * 10
}

/* class CountMacine extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      num: 1
    };
  }
  increaseValue = () => {
    // console.log("increaseValue", this.state.num);
    this.setState((prevState) => {
      return { num: prevState.num + 1 };
    });
  };
  render() {
    // console.log("CountMacine render");
    return (
      <section>
        <h2>Component Stop Reloading</h2>
        <h3>{(this.state.num % 3) === 0 ? this.state.num : "no need"}</h3>
        <button onClick={this.increaseValue}>increase</button>
        <hr />
        <Printer value={1} />
      </section>
    );
  }
} */

/* class Printer extends React.PureComponent {
  render() {
    console.log("Printer Class");
    return <h1>{calculation(this.props.value)}</h1>;
  }
} */


const Printer = React.memo((props) => {
  console.log("Printer functional");
  return <h1>{calculation(props.value)}</h1>;
})

// const style = ;

const CountMacine = () => {
  const [num, setNum] = React.useState(1);
  const [str, setStr] = React.useState("");
  const increaseValue = React.useCallback(() => {
    setNum(num + 1)
  }, [num]);
  const onStrChange = React.useCallback(() => {
    setStr(str + " ADD")
  }, [str]);

  const style = React.useMemo(() => ({ color: "red" }), []);

  return (
    <section>
      <h3>{num}</h3>
      <button onClick={increaseValue}>increase</button>
      <hr />
      <MyAddText value={str} />
      <MyAddButton style={style} value={str} onAdd={onStrChange} />
      <hr />
      <Printer value={1} />
    </section>
  );
}

const MyAddButton = React.memo((props) => {
  console.log("MyAddButton");
  return <button onClick={props.onAdd} >ADD</button>
})
const MyAddText = React.memo((props) => {
  console.log("MyAddText");
  return <h4 style={props.style}>{props.value ? props.value : "No Value"}</h4>
})

export default CountMacine;
