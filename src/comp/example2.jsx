import React from "react";
// talk about composition

const Main = () => {

    // get name value from backend
    return <>
        <h2>"composition" over "passing props</h2>
        <hr />
        <Container>
            <Parent>
                <User name={"MANOJ"} />
            </Parent>
        </Container>
    </>
}

const Container = (props) => {
    return (
        <div>
            Container UI
            {props.children}
        </div>
    );
};

const Parent = (props) => {
    return (
        <div>
            Parent UI
            {props.children}
        </div>
    );
};

const User = ({ name }) => {
    return <div>my name is {name}</div>;
};



export default Main;


