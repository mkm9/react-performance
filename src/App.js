import React from "react";
// import "./styles.css";

// comp
// import Example from "./comp/example1";
// import Example from "./comp/example2";
import Example from "./comp/example3";

function App() {
  // console.log("APP")
  return (
    <div className="App">
      <h1 style={{ color: "RED" }}> Lets talk about performance</h1>
      <Example />
    </div>
  );
}

export default App;
